package practice

import "testing"

func Test_longestCommonSubsequence_tabulated(t *testing.T) {
	type LCSTestSet struct {
		text1, text2   string
		expectedLength int
	}
	sets := []LCSTestSet{
		{
			text1:          "ace",
			text2:          "abcde",
			expectedLength: 3,
		},
		{
			text1:          "aggtab",
			text2:          "gxtxayb",
			expectedLength: 4,
		},
	}

	for _, set := range sets {
		t.Logf("LCS of '%s' and '%s'", set.text1, set.text2)
		length, subsequence := longestCommonSubsequence_tabulated(set.text1, set.text2)
		if length != set.expectedLength {
			t.Fatalf("received '%s' with length %v, expected length %v", subsequence, length, set.expectedLength)
		} else {
			t.Logf("received '%s' with length %v, expected %v", subsequence, length, set.expectedLength)
		}
	}
}

func Test_sortArray_bubbleSort(t *testing.T) {
	inputArray := []int{7, 2, 3, 5, 1, 8, 0, 9, 4, 6}
	result := sortArray_bubbleSort(inputArray)
	t.Logf("input %v, received %v", inputArray, result)
}

func Test_sortArray_selectionSort(t *testing.T) {
	inputArray := []int{7, 2, 3, 5, 1, 8, 0, 9, 4, 6}
	result := sortArray_selectionSort(inputArray)
	t.Logf("input %v, received %v", inputArray, result)
}

func Test_sortArray_insertionSort(t *testing.T) {
	inputArray := []int{7, 2, 3, 5, 1, 8, 0, 9, 4, 6}
	result := sortArray_insertionSort(inputArray)
	t.Logf("input %v, received %v", inputArray, result)
}

func Test_sortArray_mergeSort(t *testing.T) {
	inputArray := []int{7, 2, 3, 5, 1, 8, 0, 9, 4, 6}
	result := sortArray_mergeSort(inputArray)
	t.Logf("input %v, received %v", inputArray, result)
}

func Test_arrays(t *testing.T) {
	arr1 := []int{1, 2, 3}
	arr2 := arr1[0:]
	arr2[0] = 7
	t.Logf("arr1 %v, arr2 %v", arr1, arr2)
}

func Test_twoSum(t *testing.T) {
	numbers := []int{2, 7, 11, 15}
	target := 9

	result := twoSum(numbers, target)
	t.Logf("input %v, target %v, received %v", numbers, target, result)
}
