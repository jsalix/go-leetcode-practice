package practice

func longestCommonSubsequence_tabulated(text1, text2 string) (length int, subsequence string) {
	// avoid recalculating len each time
	text1Len := len(text1)
	text2Len := len(text2)

	// create empty 2-dimensional array
	state := make([][]int, text1Len+1)
	for i := 0; i <= text1Len; i++ {
		state[i] = make([]int, text2Len+1)
	}

	for x := 0; x <= text1Len; x++ {
		for y := 0; y <= text2Len; y++ {
			if x == 0 || y == 0 {
				// insert buffer values for first row and column
				// better than checking x=0||y=0 explicitly in the following if
				// but does use a little more memory
				state[x][y] = 0
			} else if text1[x-1] == text2[y-1] {
				// check previous LCS (if any) and add one
				state[x][y] = state[x-1][y-1] + 1
			} else {
				// no match but propagate the last known LCS
				state[x][y] = func(a, b int) int {
					if a > b {
						return a
					} else {
						return b
					}
				}(state[x-1][y], state[x][y-1])
			}
		}
	}
	// the final value should be the LCS length
	length = state[text1Len][text2Len]

	// calculate the subsequence
	// navigate state from the max value
	x := text1Len
	y := text2Len
	// make rune array to hold characters
	si := length - 1
	s := make([]rune, length)
	// once si is less than zero, we're done
	for si >= 0 {
		if text1[x-1] == text2[y-1] {
			// since we're starting from the last character, store in reverse
			s[si] = rune(text1[x-1])
			si--
		}

		// store current x and y to avoid comparing wrong values
		currX := x
		currY := y
		if state[currX-1][currY] >= state[currX][currY-1] {
			x--
		}
		if state[currX][currY-1] >= state[currX-1][currY] {
			y--
		}
	}
	// convert rune array to string
	subsequence = string(s)

	return
}

// this runs in O(N^2)
func sortArray_bubbleSort(inputArray []int) (sortedArray []int) {
	length := len(inputArray)
	sortedArray = make([]int, length)
	copy(sortedArray, inputArray)

	for x := 0; x < length; x++ {
		for y := 0; y < length; y++ {
			if sortedArray[x] < sortedArray[y] {
				sortedArray[x], sortedArray[y] = sortedArray[y], sortedArray[x]
			}
		}
	}

	return
}

// also O(N^2)
func sortArray_selectionSort(arr []int) []int {
	length := len(arr)
	res := make([]int, length)
	copy(res, arr)

	for L := 0; L < length-1; L++ {
		leastValue := res[L]
		leastIndex := L
		for X := L; X < length; X++ {
			if res[X] < leastValue {
				leastValue = res[X]
				leastIndex = X
			}
		}
		res[L], res[leastIndex] = res[leastIndex], res[L]
	}

	return res
}

// O(N^2)
func sortArray_insertionSort(inputArray []int) (sortedArray []int) {
	length := len(inputArray)
	sortedArray = make([]int, length)
	copy(sortedArray, inputArray)

	for x := 1; x < length; x++ {
		value := sortedArray[x]
		y := x - 1
		for ; y >= 0 && sortedArray[y] >= value; y-- {
			sortedArray[y+1] = sortedArray[y]
		}
		sortedArray[y+1] = value
	}

	return
}

// TODO needs work
func sortArray_mergeSort(inputArray []int) []int {
	length := len(inputArray)
	sortedArray := make([]int, length)
	copy(sortedArray, inputArray)

	if length > 1 {
		array1 := sortArray_mergeSort(sortedArray[0 : length/2])
		array2 := sortArray_mergeSort(sortedArray[length/2+1 : length])
		sortedArray = merge(array1, array2)
	}
	return sortedArray
}
func merge(inputArray1, inputArray2 []int) []int {
	array1 := make([]int, len(inputArray1))
	copy(array1, inputArray1)
	array2 := make([]int, len(inputArray2))
	copy(array2, inputArray2)
	length := len(array1) + len(array2)
	sortedArray := make([]int, length)
	var left, right, sortIndex int
	for left < len(array1) && right < len(array2) {
		if array1[left] <= array2[right] {
			sortedArray[sortIndex] = array1[left]
			left++
		} else {
			sortedArray[sortIndex] = array2[right]
			right++
		}
		sortIndex++
	}
	for left < len(array1) {
		sortedArray[sortIndex] = array1[left]
		sortIndex++
		left++
	}
	for right < len(array2) {
		sortedArray[sortIndex] = array2[right]
		sortIndex++
		right++
	}
	return sortedArray
}

// O(N^2), how can this be improved?
func twoSum(numbers []int, target int) []int {
	for x := 0; x < len(numbers); x++ {
		for y := 0; y < len(numbers); y++ {
			if numbers[x]+numbers[y] == target {
				return []int{x, y}
			}
		}
	}
	return []int{}
}
